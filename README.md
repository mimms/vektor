DaKnight - Base Version 1.0 2015/01/29
================================================

What is it? 
-----------

A base to use when starting a new project. 

Libraries
---------
- `slick.js`

  Simple slidshow.

- `jquery-2.1.1.js`

Documentation
-------------

### Node workflow w/ Grunt

In this project we use Grunt to automatize tasks as much as possible.  
It helps us with a lot of tasks such as compiling our less to css and  minifying javascript.

Read more: [Node.js](http://nodejs.org ”Node”), [Grunt tasks runner](http://gruntjs.com ”Grunt”)

#### Installation

If you havent allready installed it, you will have to intall Node.js on your machine.

Visit [Node.js](http://nodejs.org "Node") to do so.

#### Package.json

`Package.json` is one of the required files to set up Grunt. Here you list  
all dependencies you will need to execute your tasks. The tasks  
themselves are defined in the `Gruntfile.js`

**You are allways going to work in the front-end related folder eg. assets.**  
Use your command/terminal and the cd command to get to the right folder. If  
you are unsure about the content in a file you can list it by using the  
ls command.


#### Dependencies

To install all dependencies needed to run the tasks, open a command/terminal  
window and make your way to the front-end folder `cd to\the\path\where\your\package\json\is.`  

Then run an `npm install` . You may have to use `sudo npm install`.

This command will create the new folder `node_modules`. The dependencies you  
have listed in `package.json` will be fetched from `http://npmjs.org` and  
stored in your new folder. 

It is also possible to fetch all the latset versions of the dependencies by  
typing `npm install --save-dev`. This will override the versions you listed  
in `package.json`.

By doing this you might run into compatibilitiy problem, make sure you're aware of modules version.

Make sure to ignore the `node_modules` in your commits. This is important  
because modules can differ from one operative system to another.

#### Grunt tasks

This is the second required file in a Grunt project. In `Gruntfile.js` all  
tasks needed to compile, concatenate, minify your code at different steps of  
the process like local, development and stage/production are listed.

Run grunt by writing `grunt` in your command/terminal.

