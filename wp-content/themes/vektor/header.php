<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(''); ?></title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
<!--         <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/styles/components/jquery.sidr.dark.css">    -->     
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/styles/components/jquery.sidr.light.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/styles/components/flexslider.css">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.0/slick.css"/>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/styles/styles.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --> 
        <!--[if lt IE 9]> 
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script> 
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> 
        <script>alert('För att denna sida ska upplevas rätt behöver du uppdatera Internet Explorer till en nyare version.');</script>
        <![endif]--> 
        <?php wp_head(); ?>
    </head>
    <body>
